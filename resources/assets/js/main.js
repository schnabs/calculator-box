var Vue = require('vue');
var VueCharts = require('vue-charts');
var VueResource = require('vue-resource');

import Greeter from './components/Greeter.vue';
import CompoundInterestCalculator from './components/CompoundInterestCalculator.vue';
import ExchangeRateCalculator from './components/ExchangeRateCalculator.vue';
import PortfolioYieldCalculator from './components/PortfolioYieldCalculator.vue';

Vue.use(VueCharts);

Vue.use(VueResource);
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');

new Vue({
    el: '#app',


    components: { Greeter, CompoundInterestCalculator, ExchangeRateCalculator, PortfolioYieldCalculator},


    ready() {
        //alert('Vue and Vueify all set to go!');
    }
});