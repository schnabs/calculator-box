<!DOCTYPE html>
<html>
<head>
    <title>CalculatorBox</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta id="token" name="token" value="{{csrf_token()}}">

    <link rel="stylesheet" type="text/css" href="{{ elixir('css/app.css') }}">

</head>
<body>




<div id="app" class="container">


    @include('_navbar')


    @yield('content')

    <!--<greeter></greeter>-->

</div>

<script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_CHTML"></script>
<script src="{{ elixir('js/main.js') }}"></script>


</body>
</html>