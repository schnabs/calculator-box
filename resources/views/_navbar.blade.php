<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">CalculatorBox</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="/">Home</a></li>
            <li><a href="/compound-interest-calculator">Compound Interest Calculator</a></li>
            <li><a href="/exchange-rates">Exchange Rates</a></li>
            <li><a href="/portfolio-yield-calculator">Portfolio Yield</a></li>

        </ul>
    </div>
</nav>