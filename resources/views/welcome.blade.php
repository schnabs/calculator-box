@extends('master')

@section('content')
    <div class="jumbotron">
        <h1>Welcome to CalculatorBox</h1>
        <p>Building financial tools to empower you. Check out our first tool, the Compound Interest Calculator</p>
        <p><a class="btn btn-primary btn-lg" href="/compound-interest-calculator" role="button">Try it!</a></p>
    </div>

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Financial Tools</div>
        <div class="panel-body">
            <p>This is a current list of our financial tools, check back often for updates!</p>
        </div>

        <div class="list-group">
            <a href="/compound-interest-calculator" class="list-group-item">
                <h4 class="list-group-item-heading">Compound Interest Calculator</h4>
            </a>
            <a href="/exchange-rates" class="list-group-item">
                <h4 class="list-group-item-heading">Exchange Rates</h4>
            </a>
            <a href="/portfolio-yield-calculator" class="list-group-item">
                <h4 class="list-group-item-heading">Portfolio Yield</h4>
            </a>
        </div>
    </div>

@endsection