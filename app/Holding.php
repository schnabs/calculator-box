<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holding extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'symbol', 'quantity'
    ];
}
