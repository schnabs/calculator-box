<?php

Route::get('/api/exchange', 'ExchangeApiController@getFixerLatest');
Route::get('/api/quote', 'StockApiController@getStockData');

Route::resource('/api/holdings', 'HoldingsApiController');


