<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ExchangeApiController extends Controller
{
    /**
     * Return a JSON representation of exchange rates based on EUR from http://fixer.io/
     */
    public function getFixerLatest(){

        $client = new \GuzzleHttp\Client();

        $res = $client->request('GET', 'http://api.fixer.io/latest');

        //dd($res);

        //echo $res->getStatusCode();
        // 200
        //echo $res->getHeaderLine('content-type');
        // 'application/json; charset=utf8'
        //echo $res->getBody();
        // {"type":"User"...'


        $json = \GuzzleHttp\json_decode($res->getBody());

        $json->rates->{$json->base} = 1.0;


        //var_dump($json->rates);

        return json_encode($json->rates);

    }

}
