<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class StockApiController extends Controller
{
    public function getStockData(){

        $stocks = explode(',', Input::get('s') ); //?s=MSFT,TSLA,AAPL,MCD

        $stock_params = [
            's'=>'symbol',
            'n'=>'name',
            'x'=>'exchnage',
            'a'=>'ask',
            'b'=>'bid',
            'o'=>'open',
            'p'=>'previous_close',
            'y'=>'yield',
            //''=>'',
        ];
        list($request_params, $request_param_names) = array_divide($stock_params);

        //http://wern-ancheta.com/blog/2015/04/05/getting-started-with-the-yahoo-finance-api/
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'http://finance.yahoo.com/d/quotes.csv', [
            'query' => [
                's'=> implode(',', $stocks),
                'f'=> implode('', $request_params)
            ]
        ]);
        //dd($res);

        //echo $res->getStatusCode();
        // 200
        //echo $res->getHeaderLine('content-type');
        // 'application/json; charset=utf8'
        //echo $res->getBody();
        // {"type":"User"...'

        $body = $res->getBody();
        $stock_data=[];

        if($body->getSize() > 1){
            $stock_results = str_getcsv($body, PHP_EOL);
            //var_dump($stock_results);

            foreach ($stock_results as $stock_result) {
                $parts = str_getcsv($stock_result);
                $parts = array_map( function($x) { return $x == 'N/A' ? '0' : $x; }, $parts);
                //var_dump($parts);
                $named_parts = array_combine($request_param_names, $parts);
                //var_dump($named_part);
                $stock_data[]=$named_parts;
            }
        }

        return json_encode($stock_data);
    }
}
