<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/compound-interest-calculator', function () {
    return view('cic');
});

Route::get('/exchange-rates', function () {
    return view('exchangerates');
});

Route::get('/portfolio-yield-calculator', function () {
    return view('portfolioyield');
});




Route::resource('/webapi/holdings', 'HoldingsApiController');

